package osotnikov.pleasantries;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PleasantriesApplication {

	public static void main(String[] args) {
		SpringApplication.run(PleasantriesApplication.class, args);
	}

}
