package osotnikov.greeter.endpoint;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/greeting")
public class GreeterController {

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class GreetingResponse {
        private String greeting = "Hello";
    }

    @GetMapping("/{name}")
    public GreetingResponse getTrooper(@PathVariable("name") String name) {
        GreetingResponse greetingResponse = new GreetingResponse();
        greetingResponse.setGreeting(greetingResponse.getGreeting() + ", " + name);
        return greetingResponse;
    }

}
